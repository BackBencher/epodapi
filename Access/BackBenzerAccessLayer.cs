﻿// ===============================
// AUTHOR     : SAVEEN GURUNG
// CREATE DATE     : 06/June/2018
// PURPOSE     : Accessing database 
// SPECIAL NOTES:
// ===============================
// Change History:
//
//==================================

using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Text;

namespace Access {
    public class BackBenzerAccessLayer {
        //private static string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
        private static string connectionString = ConfigurationManager.AppSettings["ConStr"].ToString();
        private static Type type;

        private delegate void executeProceduresWithoutResultDelegate(string procedureName, Hashtable hashParam);

        /// <summary>
        /// Accepts procedurename and HashTable Parameters and return Datatable
        /// </summary>
        /// <param name="procedureName"></param>
        /// <param name="hashParam"></param>
        /// <returns>DataTable</returns>
        public static DataTable executeProceduresWithResult(string procedureName, Hashtable hashParam) {
            SqlConnection con = new SqlConnection(connectionString);
            if (procedureName == null) {
                throw new System.ArgumentNullException("ProcedureName cannot be null");
            }
            if (procedureName.Trim().Equals("")) {
                throw new System.ArgumentException("ProcedureName cannot be blank");
            }
            SqlParameter sqlParam = null;
            SqlCommand cmd = new SqlCommand();
            cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = procedureName;
            if (hashParam != null) {
                if (hashParam.Count == 0) {
                    throw new System.ArgumentException("HashTable size cannot be zero");
                }
                foreach (DictionaryEntry dic in hashParam) {
                    sqlParam = new SqlParameter();
                    sqlParam.ParameterName = dic.Key.ToString();
                    Type type = (dic.Value==null)? null: dic.Value.GetType();
                    sqlParam.SqlDbType = getParameterType(type);
                    sqlParam.Value = (dic.Value == DBNull.Value || dic.Value == null) ? DBNull.Value : dic.Value;
                    cmd.Parameters.Add(sqlParam);
                }
            }
            DataTable dataTable;
            try {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
                dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
            } catch (Exception ex) {
                throw ex;
            } finally {
                cmd.Dispose();
                cmd = null;
                con.Close();
            }
            return dataTable;
        }


        /// <summary>
        /// Accepts procedurename and HashTable Parameters and return Datatable
        /// </summary>
        /// <param name="procedureName"></param>
        /// <param name="hashParam"></param>
        /// <returns>DataSet</returns>
        public static DataSet executeProceduresWithResultDataSet(string procedureName, Hashtable hashParam) {
            SqlConnection con = new SqlConnection(connectionString);
            if (procedureName == null) {
                throw new System.ArgumentNullException("ProcedureName cannot be null");
            }
            if (procedureName.Trim().Equals("")) {
                throw new System.ArgumentException("ProcedureName cannot be blank");
            }
            SqlParameter sqlParam = null;
            SqlCommand cmd = new SqlCommand();
            cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = procedureName;
            if (hashParam != null) {
                if (hashParam.Count == 0) {
                    throw new System.ArgumentException("HashTable size cannot be zero");
                }
                foreach (DictionaryEntry dic in hashParam) {
                    sqlParam = new SqlParameter();
                    sqlParam.ParameterName = dic.Key.ToString();
                    Type type = dic.Value.GetType();
                    sqlParam.SqlDbType = getParameterType(type);
                    sqlParam.Value = (dic.Value == DBNull.Value || dic.Value == null) ? DBNull.Value : dic.Value;
                    cmd.Parameters.Add(sqlParam);
                }
            }
            DataSet ds;
            try {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
                ds = new DataSet();
                dataAdapter.Fill(ds);
            } catch (Exception ex) {
                throw ex;
            } finally {
                cmd.Dispose();
                cmd = null;
                con.Close();
            }
            return ds;
        }

        public static void ExecuteProceduresWithoutResult(string procedureName, Hashtable hashParam) {
            SqlConnection con = new SqlConnection(connectionString);
            if (procedureName == null) {
                throw new System.ArgumentNullException("ProcedureName cannot be null");
            }
            if (procedureName.Trim().Equals("")) {
                throw new System.ArgumentException("ProcedureName cannot be blank");
            }
            SqlParameter sqlParam = null;
            SqlCommand cmd = new SqlCommand();
            cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = procedureName;
            if (hashParam != null) {
                if (hashParam.Count == 0) {
                    throw new System.ArgumentException("HashTable size cannot be zero");
                }
                foreach (DictionaryEntry dic in hashParam) {
                    sqlParam = new SqlParameter();
                    sqlParam.ParameterName = dic.Key.ToString();
                    Type type = dic.Value.GetType();
                    sqlParam.SqlDbType = getParameterType(type);
                    sqlParam.Value = (dic.Value == DBNull.Value || dic.Value == null) ? DBNull.Value : dic.Value;
                    cmd.Parameters.Add(sqlParam);
                }
            }

            try {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                if (con.State == ConnectionState.Open)
                    cmd.ExecuteNonQuery();
            } catch (Exception ex) {
                throw ex;
            } finally {
                cmd.Dispose();
                cmd = null;
                con.Close();
            }
        }


        /// <summary>
        /// Accepts procedurename and HashTable only, returns nothing
        /// </summary>
        /// <param name="procedureName"></param>
        /// <param name="hashParam"></param>
        /// <returns>Void</returns>
        public static void asyncExecuteProceduresWithoutResult(string procedureName, Hashtable hashParam) {
            executeProceduresWithoutResultDelegate execDelegate = new executeProceduresWithoutResultDelegate(executeProceduresWithoutResult);
            execDelegate.BeginInvoke(procedureName, hashParam, null, null);
        }

        public static string getDropDownList(string tableName, string cond, string valuePart, string textPart) {
            StringBuilder strBuild = new StringBuilder();
            string[,] Parameters = new string[,]{
                {"@Cond",cond.Trim()},
                {"@TableName",tableName.Trim()},
                {"@ValuePart",valuePart.Trim()},
                {"@TextPart",textPart.Trim()}
            };
            Hashtable hashTable = new Hashtable();
            hashTable.Add("@Cond", cond.Trim());
            hashTable.Add("@TableName", tableName.Trim());
            hashTable.Add("@ValuePart", valuePart.Trim());
            hashTable.Add("@TextPart", textPart.Trim());
            DataTable dt = executeProceduresWithResult("SP_BindDropDownList", hashTable);
            if (dt.Rows.Count > 0) {
                for (int i = 0; i < dt.Rows.Count; ++i) {
                    strBuild.Append("<option value=" + dt.Rows[i]["ValuePart"] + ">" + dt.Rows[i]["TextPart"] + "</option>");
                }
            }
            return strBuild.ToString();
        }

        public static DataTable BindDropDownList(string Cond, string TableName, string ValuePart, string TextPart) {
            Hashtable hashTable = new Hashtable();
            hashTable.Add("@Cond", Cond.Trim());
            hashTable.Add("@TableName", TableName.Trim());
            hashTable.Add("@ValuePart", ValuePart.Trim());
            hashTable.Add("@TextPart", TextPart.Trim());
            return executeProceduresWithResult("SP_BindDropDownList", hashTable);
        }

        private static void executeProceduresWithoutResult(string procedureName, Hashtable hashParam) {
            SqlConnection con = new SqlConnection(connectionString);
            if (procedureName == null) {
                throw new System.ArgumentNullException("ProcedureName cannot be null");
            }
            if (procedureName.Trim().Equals("")) {
                throw new System.ArgumentException("ProcedureName cannot be blank");
            }
            if (hashParam.Count == 0) {
                throw new System.ArgumentException("HashTable size cannot be zero");
            }
            if (hashParam == null) {
                throw new System.ArgumentNullException("HashTable cannot be null");
            }
            SqlParameter sqlParam = null;
            SqlCommand cmd = new SqlCommand();
            cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = procedureName;
            foreach (DictionaryEntry dic in hashParam) {
                sqlParam = new SqlParameter();
                sqlParam.ParameterName = dic.Key.ToString();
                Type type = dic.Value.GetType();
                sqlParam.SqlDbType = getParameterType(type);
                sqlParam.Value = (dic.Value == DBNull.Value || dic.Value == null) ? DBNull.Value : dic.Value;
                cmd.Parameters.Add(sqlParam);
            }
            try {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                cmd.ExecuteNonQueryAsync();
            } catch (Exception ex) {
                throw ex;
            } finally {
                cmd.Dispose();
                cmd = null;
                con.Close();
            }
        }

        public static SqlDbType getParameterType(Type type) {
            SqlDbType paramType = SqlDbType.VarChar;
            switch (Type.GetTypeCode(type)) {
                case TypeCode.Int32:
                    paramType = SqlDbType.Int;
                    break;

                case TypeCode.String:
                    paramType = SqlDbType.VarChar;
                    break;

                case TypeCode.Double:
                    paramType = SqlDbType.Decimal;
                    break;

                case TypeCode.DateTime:
                    paramType = SqlDbType.DateTime;
                    break;

                case TypeCode.Boolean:
                    paramType = SqlDbType.Bit;
                    break;

                case TypeCode.Byte:
                    paramType = SqlDbType.Image;
                    break;
            }
            return paramType;
        }

        public static List<T> ConvertDataTableToList<T>(DataTable dt) {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows) {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        private static T GetItem<T>(DataRow dr) {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns) {
                foreach (PropertyInfo pro in temp.GetProperties()) {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
    }
}
