﻿using DAL;
using Logger;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;

namespace BAL {
    public class BusinessLogic {

        public UserDetail Login(out int status_code, out string message,Login login) {
            status_code = 0;
            message = AppMessage.RECORD_NOT_FOUND;
            try {
                login.password = CryptoEngine.Encrypt(login.password);

                DataTable dataTable = new DataAccessLayer().Login(login);

                if (dataTable != null && dataTable.Rows.Count > 0) {
                    UserDetail user = dataTable.ToListof<UserDetail>().First();
                    status_code = 1;
                    message = AppMessage.RECORD_FOUND;
                    return user;
                }

            } catch (Exception ex) {
                SVNLogger.Log(LogType.ERROR, ex.ToString());
                throw;
            }
            return null;
        }

        public List<Packets> GetPackets(out int status_code, out string message, string imei_no) {
            status_code = 0;
            message = AppMessage.RECORD_NOT_FOUND;
            try {
                DataTable dataTable = new DataAccessLayer().GetPackets(imei_no);

                if (dataTable != null && dataTable.Rows.Count > 0) {
                    List<Packets> userList = dataTable.ToListof<Packets>();
                    status_code = 1;
                    message = AppMessage.RECORD_FOUND;
                    return userList;
                }

            } catch (Exception ex) {
                message = ex.Message;
                SVNLogger.Log(LogType.ERROR, ex.ToString());
            }
            return null;
        }

        public Dictionary<string, object> GetMobileData(out int status_code, out string message) {
            status_code = 0;
            message = AppMessage.RECORD_NOT_FOUND;
            try {
                DataSet dataSet = new DataAccessLayer().GetMobileData();

                if (dataSet != null && dataSet.Tables.Count == 3) {
                    Dictionary<string, object> dictionary = new Dictionary<string, object>();
                    dictionary.Add("remarks", dataSet.Tables[0].ToListof<PacketRemarks>());
                    dictionary.Add("relation", dataSet.Tables[1].ToListof<PacketRelation>());
                    dictionary.Add("status", dataSet.Tables[2].ToListof<PacketStatusType>());

                    status_code = 1;
                    message = AppMessage.RECORD_FOUND;

                    return dictionary;
                }

            } catch (Exception ex) {
                message = ex.Message;
                SVNLogger.Log(LogType.ERROR, ex.ToString());
            }
            return null;
        }

        public string GetAndroidVersion(out int status_code, out string message) {
            status_code = 0;
            message = AppMessage.RECORD_NOT_FOUND;
            try {
                DataTable dataTable = new DataAccessLayer().GetAndroidVersion();

                if (dataTable != null && dataTable.Rows.Count > 0) {
                    status_code = 1;
                    message = AppMessage.RECORD_FOUND;

                    return dataTable.Rows[0]["AndroidVersion"].ToString();
                }

            } catch (Exception ex) {
                message = ex.Message;
                SVNLogger.Log(LogType.ERROR, ex.ToString());
            }
            return null;
        }

        public Dictionary<string,object> UpdatePacket(out int status_code, out string message, UpdatePacket updatePacket) {
            status_code = 0;
            message = AppMessage.RECORD_NOT_FOUND;
            try {
                DataTable dataTable = new DataAccessLayer().UpdatePacket(updatePacket);

                if (dataTable != null && dataTable.Rows.Count > 0) {

                    Dictionary<string, object> dictionary = new Dictionary<string, object>();
                    dictionary.Add("feed", dataTable.Rows[0]["feed"].ToString());
                    dictionary.Add("delivery_status", dataTable.Rows[0]["DeliveryStatus"].ToString());
                    dictionary.Add("awb_no", dataTable.Rows[0]["AwbNo"].ToString());
                    
                    status_code = 1;
                    message = AppMessage.RECORD_FOUND;

                    return dictionary;
                }

            } catch (Exception ex) {
                message = ex.Message;
                SVNLogger.Log(LogType.ERROR, ex.ToString());
            }
            return null;
        }

    }
}
