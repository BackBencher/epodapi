﻿using Access;
using Logger;
using Model;
using System;
using System.Collections;
using System.Data;


namespace DAL {
    public class DataAccessLayer {

        public DataTable Login(Login login) {
            try {
                Hashtable hashtable = new Hashtable();
                hashtable.Add("@EmpCode", login.empcode);
                hashtable.Add("@Password", login.password);
                return BackBenzerAccessLayer.executeProceduresWithResult("Sp_Android_GetLoginDetail", hashtable);
            } catch (Exception ex) {
                SVNLogger.Log(LogType.ERROR, ex.ToString());
                throw;
            }
        }

        public DataTable GetPackets(string imei_no) {

            try {
                Hashtable hashtable = new Hashtable();
                hashtable.Add("@IEMINo", imei_no);
                return BackBenzerAccessLayer.executeProceduresWithResult("SP_Android_GetPacketsOnIEMI", hashtable);
            } catch (Exception ex) {
                SVNLogger.Log(LogType.ERROR, ex.ToString());
                throw;
            }
        }

        public DataSet GetMobileData() {
            try {
                return BackBenzerAccessLayer.executeProceduresWithResultDataSet("sp_Android_MobileData", null);
            } catch (Exception ex) {
                SVNLogger.Log(LogType.ERROR, ex.ToString());
                throw;
            }
        }

        public DataTable GetAndroidVersion() {
            try {
                return BackBenzerAccessLayer.executeProceduresWithResult("Sp_Android_GetCurrentAndroidVersion", null);
            } catch (Exception ex) {
                SVNLogger.Log(LogType.ERROR, ex.ToString());
                throw;
            }
        }

        public DataTable UpdatePacket(UpdatePacket updatePacket) {
            try {
                Hashtable hashtable = new Hashtable();
                hashtable.Add("@AwbNo", updatePacket.awb_no);
                hashtable.Add("@Delivery_Status", updatePacket.delivery_status);
                hashtable.Add("@CodAmount", updatePacket.cod_amount);
                hashtable.Add("@DeliveryBoy", updatePacket.delivery_boy);
                hashtable.Add("@RCName", updatePacket.rc_name);
                hashtable.Add("@RcRelation", updatePacket.rc_relation);
                hashtable.Add("@PaymentReceiveMode", updatePacket.payment_recieve_mode);
                hashtable.Add("@Rto_Reason", updatePacket.rto_reason);
                hashtable.Add("@Latitude", updatePacket.latitude);
                hashtable.Add("@Longitude", updatePacket.longitude);
                hashtable.Add("@RcPhoneNo", updatePacket.rc_phone_no);
                hashtable.Add("@IDProof", updatePacket.id_proof);
                hashtable.Add("@IDProofNo", updatePacket.id_proof_no);
                hashtable.Add("@PodImage", Convert.FromBase64String(updatePacket.pod_image));
                hashtable.Add("@Signature", updatePacket.signature);
                hashtable.Add("@PodRemarks", updatePacket.pod_remarks);
                hashtable.Add("@ReceiverPic", Convert.FromBase64String(updatePacket.receiver_pic));
                return BackBenzerAccessLayer.executeProceduresWithResult("Sp_Android_UpdatePacket", hashtable);
            } catch (Exception ex) {
                SVNLogger.Log(LogType.ERROR, ex.ToString());
                throw;
            }
        }



    }
}
