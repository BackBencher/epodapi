﻿using BAL;
using Model;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace EPODAPI.Controllers {
    public class EPODController : ApiController {

        [HttpPost]
        public HttpResponseMessage Login(Login login) {
            if (!ModelState.IsValid) {
                var errors = ModelState.Select(x => new Dictionary<string, string> { { x.Key, x.Value.Errors.First().ErrorMessage } })
                           .Where(y => y.Count > 0)
                           .ToList();
                return Request.CreateResponse(HttpStatusCode.OK, errors);
            }

            int http_status = 0;
            string message = string.Empty;

            UserDetail user = new BusinessLogic().Login(out http_status, out message, login);

            return Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object> {
                { AppMessage.STATUS, http_status },
                { AppMessage.MESSAGE, message },
                { AppMessage.USER_DATA, user }
            });
        }


        [HttpGet]
        public HttpResponseMessage GetPackets() {
            HttpRequestHeaders header = Request.Headers;

            if (!header.Contains("imei_no")) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, string>() { { AppMessage.MESSAGE, AppMessage.REQUEST_PARAM_MISSING } });
            }

            int http_status = 0;
            string message = string.Empty;

            List<Packets> packets = new BusinessLogic().GetPackets(out http_status, out message, header.GetValues("imei_no").First());

            return Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object> {
                { AppMessage.STATUS, http_status },
                { AppMessage.MESSAGE, message },
                { AppMessage.PACKETS, packets }
            });
        }

        [HttpGet]
        public HttpResponseMessage GetMobileData() {
            int http_status =0;
            string message = string.Empty;

            Dictionary<string, object> mobile_data = new BusinessLogic().GetMobileData(out http_status, out message);

            return Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object> {
                { AppMessage.STATUS, http_status },
                { AppMessage.MESSAGE, message },
                { AppMessage.MOBILE_DATA, mobile_data }
            });
        }

        [HttpGet]
        public HttpResponseMessage GetAndroidVersion() {
            int http_status = 0;
            string message = string.Empty;

            string android_version = new BusinessLogic().GetAndroidVersion(out http_status, out message);

            return Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object> {
                { AppMessage.STATUS, http_status },
                { AppMessage.MESSAGE, message },
                { AppMessage.ANDROID_VERSION, android_version }
            });
        }

        [HttpPost]
        public HttpResponseMessage UpdatePacket(UpdatePacket packet) {
            if (!ModelState.IsValid) {
                var errors = ModelState.Select(x => new Dictionary<string, string> { { x.Key, x.Value.Errors.First().ErrorMessage } })
                           .Where(y => y.Count > 0)
                           .ToList();
                return Request.CreateResponse(HttpStatusCode.OK, errors);
            }

            int http_status = 0;
            string message = string.Empty;

            Dictionary<string, object> dictionary = new BusinessLogic().UpdatePacket(out http_status, out message, packet);

            return Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object> {
                { AppMessage.STATUS, http_status },
                { AppMessage.MESSAGE, message },
                { AppMessage.PACKET_FEED, dictionary }
            });
        }

    }
}