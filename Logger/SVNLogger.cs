﻿
namespace Logger {
    public class SVNLogger {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void Log(LogType type, string message) {
            
            switch (type) {

                case LogType.INFO:
                    log.Info(message);
                    break;

                case LogType.DEBUG:
                    log.Debug(message);
                    break;

                case LogType.ERROR:
                    log.Error(message);
                    break;

                case LogType.FATEL:
                    log.Fatal(message);
                    break;

                case LogType.WARN:
                    log.Warn(message);
                    break;
                    
            }
        }
    }

    public enum LogType {
        INFO = 0,
        DEBUG =1,
        ERROR =2,
        WARN=3,
        FATEL=4
    }
}
