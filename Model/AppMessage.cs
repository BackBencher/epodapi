﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model {
  public class AppMessage {

        public static string STATUS = "status";

        public static string MESSAGE = "message";

        public static string RECORD_SAVED = "Record has been saved successfully";

        public static string RECORD_NOT_SAVED = "Record could not saved please try again";

        public static string RECORD_NOT_FOUND = "Record not found";

        public static string RECORD_FOUND = "Record found";

        public static string USER_DATA = "user_detail";

        public static string REQUEST_PARAM_MISSING = "Requested parameter missing";

        public static string PACKETS = "packets";

        public static string MOBILE_DATA = "mobile_data";

        public static string ANDROID_VERSION = "android_version";

        public static string PACKET_FEED = "packet_feed";
    }
}
