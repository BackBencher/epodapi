﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model {
    public class Login {

        [Required(ErrorMessage ="Empcode is required")]
        public string empcode { get; set; }

        [Required(ErrorMessage = "Password is required")]
        public string password { get; set; }
        
    }
}
