﻿using Newtonsoft.Json;

namespace Model {
    public class PacketRelation {

        [JsonProperty("rc_relation_code")]
        public string RcRelationCode { get; set; }

        [JsonProperty("rc_relation")]
        public string RcRelation { get; set; }

    }
}
