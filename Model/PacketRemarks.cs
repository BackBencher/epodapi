﻿using Newtonsoft.Json;

namespace Model {
    public class PacketRemarks {
        
        [JsonProperty("rc_remarks_code")]
        public string RcRemarksCode { get; set; }

        [JsonProperty("rc_remarks")]
        public string RcRemarks { get; set; }

    }
}
