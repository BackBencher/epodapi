﻿using Newtonsoft.Json;

namespace Model {
   public class PacketStatusType {

        [JsonProperty("packet_status_code")]
        public string PacketStatusCode { get; set; }

        [JsonProperty("packet_status")]
        public string PacketStatus { get; set; }
    }
}
