﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model {
    public class Packets {

        [JsonProperty("consignee")]
        public string Consignee { get; set; }

        [JsonProperty("awb_no")]
        public string AwbNo { get; set; }

        [JsonProperty("invoice_value")]
        public decimal Invoice_Value { get; set; }

        [JsonProperty("payment_mode")]
        public string PaymentMode { get; set; }

        [JsonProperty("consignee_address")]
        public string ConsigneeAddress { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("is_down")]
        public bool IsDownonPDA { get; set; }

        [JsonProperty("runsheet_date")]
        public string RunsheetDate { get; set; }

        [JsonProperty("server_date")]
        public string ServerDate { get; set; }

        [JsonProperty("runsheet")]
        public decimal RunSheet { get; set; }

        [JsonProperty("order_type")]
        public string OrderType { get; set; }

        [JsonProperty("pickup_address")]
        public string PickUpAddress { get; set; }

        [JsonProperty("response")]
        public string Response { get; set; }

        [JsonProperty("distance")]
        public int DistanceM { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("pin")]
        public string Pin { get; set; }
    }
}
