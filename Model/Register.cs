﻿using System.ComponentModel.DataAnnotations;

namespace Model {
    public class Register {

        [Required(ErrorMessage = "User Name is required")]
        [MaxLength(50,ErrorMessage ="User Name cannot be greater than 50 character")]
        public string user_name { get; set; }
        
        [Required(ErrorMessage = "Password is required")]
        public string password { get; set; }

        [Required(ErrorMessage ="First Name is required")]
        [MaxLength(50, ErrorMessage = "First Name cannot be greater than 50 character")]
        public string first_name { get; set; }

        [Required(ErrorMessage = "Last Name is required")]
        [MaxLength(50, ErrorMessage = "Last Name cannot be greater than 50 character")]
        public string last_name { get; set; }

        [Required(ErrorMessage = "Email Address is required")]
        [MaxLength(100, ErrorMessage = "Email Address cannot be greater than 100 character")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string email_address { get; set; }

        [Required(ErrorMessage = "Phone No is required")]
        [MaxLength(20, ErrorMessage = "Phone cannot be greater than 20 character")]
        [DataType(DataType.EmailAddress)]
        public string phone_no { get; set; }


        public string registration_source { get; set; }
        
        public string facebook_token { get; set; }

        public string google_token { get; set; }

        public bool term_condition_accepted { get; set; }
  
    }
}
