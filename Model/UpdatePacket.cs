﻿using System.ComponentModel.DataAnnotations;

namespace Model {
    public class UpdatePacket {

        [Required(ErrorMessage = "awb_no is required")]
        public string awb_no { get; set; }

        [Required(ErrorMessage = "delivery_status is required")]
        [MaxLength(20, ErrorMessage = "delivery_status cannot be greater than 20 character")]
        public string delivery_status { get; set; }

        public decimal cod_amount { get; set; }

        public string delivery_boy { get; set; }

        public string rc_name { get; set; }

        public string rc_relation { get; set; }

        public string payment_recieve_mode { get; set; }

        public string rto_reason { get; set; }

        public string latitude { get; set; }

        public string longitude { get; set; }

        public string rc_phone_no { get; set; }

        public string id_proof { get; set; }

        public string id_proof_no { get; set; }

        public string pod_image { get; set; }

        public string signature { get; set; }

        public string pod_remarks { get; set; }

        public string receiver_pic { get; set; }

    }
}
