﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model {
   public class UserDetail {

        [JsonProperty("ecode")]
        public string ECode { get; set; }

        [JsonProperty("name")]
        public string EName { get; set; }
    }
}
